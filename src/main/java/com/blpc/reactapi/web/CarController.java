package com.blpc.reactapi.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blpc.reactapi.domain.Car;
import com.blpc.reactapi.domain.CarRepository;

@RestController
public class CarController {
  @Autowired
  private CarRepository repository;
  
  @RequestMapping("/cars")
  public Iterable<Car> getCars() {
    return repository.findAll();
  }
}