package com.blpc.reactapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.blpc.reactapi.domain.Car;
import com.blpc.reactapi.domain.CarRepository;
import com.blpc.reactapi.domain.Owner;
import com.blpc.reactapi.domain.OwnerRepository;
import com.blpc.reactapi.domain.User;
import com.blpc.reactapi.domain.UserRepository;


@SpringBootApplication
public class ReactapiApplication {
  @Autowired 
  private CarRepository repository;
  
  @Autowired 
  private OwnerRepository orepository;

  @Autowired 
  private UserRepository urepository;
  
  public static void main(String[] args) {
		// after adding this comment 
		SpringApplication.run(ReactapiApplication.class, args);
	}
	
	@Bean

    CommandLineRunner runner(){
		return args -> {
	     // Add owner objects and save these to db
      Owner owner1 = new Owner("Random", "Rules");
      Owner owner2 = new Owner("Dude", "Ben");
      orepository.save(owner1);
      orepository.save(owner2);

      // Add car object with link to owners and save these to db.
      repository.save(new Car("Buick", "Fox", "Brown",
          "WTG-4534", 2019, 424242, owner1));
      repository.save(new Car("Edsel", "Tennessee", "Blue",
          "SSR-9093", 2018, 17234, owner2));
      repository.save(new Car("Knickerbocker", "Knacker", "Black",
          "CCR-1337", 2020, 12345, owner2));
      
      urepository.save(new User("blpcuser", 
          "$2y$12$khT.XF1.gJwgPGrnWddhGez6REEgLJ25leRhba7iR.gY1kaAEKLze",
          "USER"));
      urepository.save(new User("blpcadmin", 
          "$2y$12$BzrIiCQATPHC11xUPWrJeuXPCbmDBh9ubiS8cvS1kD3gEJCAeaOMi",
          "ADMIN"));
      };
    } 

}
